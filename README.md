# Wedoogift

## Gift & Meal deposits

### Mon approche
Pour repondre à la demande une API tournant autour de 4 services/entités est créé

* Company
* Gift
* Meal
* Member

Un service supplementaire est créé pour simuler les changements de date
* Time

### L'api

1. Le path Company 
Le path principal est le path company, il a pour but de pemettre de rajouter un *gift* ou un *meal* au member d'une company
```
POST : http://localhost:8080/api/company/{idCompany}/member/{idMember}/gift + rajouter le montant du gift dans le body
POST : http://localhost:8080/api/company/{idCompany}/member/{idMember}/meal + rajouter le montant du meal dans le body
```
2. le path Time
Le path time permets de simuler le temps qui passe pour l'exercice
```
GET : http://localhost:8080/api/time/   Recupere la date 'actuelle'
GET : http://localhost:8080/api/time/add/days/365  Rajoute X jours à la date 'actuelle'
```


### Test cumcumber

Des tests cucumbers ont été écrient. 
Les tests cucumbers se lance comme les tests j-unit.