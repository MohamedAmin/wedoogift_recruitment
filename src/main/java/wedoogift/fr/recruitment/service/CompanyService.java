package wedoogift.fr.recruitment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Company;
import wedoogift.fr.recruitment.entity.Gift;
import wedoogift.fr.recruitment.entity.Meal;
import wedoogift.fr.recruitment.entity.Member;
import wedoogift.fr.recruitment.repository.CompanyRepository;
import wedoogift.fr.recruitment.repository.MemberRepository;


@Service
public class CompanyService {
	private final CompanyRepository companyRepository;
	private final MemberRepository memberRepository;

	@Autowired
	public CompanyService(CompanyRepository companyRepository, MemberRepository memberRepository) {
		this.companyRepository = companyRepository;
		this.memberRepository = memberRepository;
	}

	public List<Company> getCompanys() {
		return companyRepository.findAll();
	}

	
	@Transactional
	public void addCompany(String name) {
		Company c = new Company();
		c.setName(name);
		this.companyRepository.save(c);
	}

	public void addGiftToMember(Long id, Long idMember, Integer montant) {
		Company company = this.companyRepository.findById(id);
		Member member = company.getMembers().stream().filter(m -> m.getId().equals(idMember)).findFirst().get();
		member.addGift(new Gift(montant));
		memberRepository.save(member);
	}

	public Company getCompany(Long id) {
		return this.companyRepository.findById(id);
	}

	public void addMealToMember(Long id, Long idMember, Integer montant) {
		Company company = this.companyRepository.findById(id);
		Member member = company.getMembers().stream().filter(m -> m.getId().equals(idMember)).findFirst().get();
		member.addMeal(new Meal(montant));
		memberRepository.save(member);			
	}

}
