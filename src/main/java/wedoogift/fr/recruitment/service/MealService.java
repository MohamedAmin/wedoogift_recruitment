package wedoogift.fr.recruitment.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Meal;
import wedoogift.fr.recruitment.repository.MealRepository;

@Service
public class MealService {

	private final MealRepository mealRepository;
	
	private final TimeManager timeManager;

	@Autowired
	public MealService(MealRepository mealRepository, TimeManager timeManager) {
		this.mealRepository = mealRepository;
		this.timeManager = timeManager;
	}

	public List<Meal> getMeals() {
		
		return mealRepository.findAll();
	}

	public Meal getMeal(Long id) {
		
		return mealRepository.findById(id);
	}
	
	@Transactional
	public void delete(Long id) {
		mealRepository.deleteById(id);
	}

	public void addGift(String libelle, Integer montant) {
		Meal meal = new Meal();
		meal.setMontant(montant);
		this.mealRepository.save(meal);
	}
	
	@Transactional
	public void updateAllMeal() {
		List<Meal> meals = mealRepository.findAllActive()
				.stream()
				.filter(m -> m.getEndDate().isBefore(timeManager.getDate()))
				.collect(Collectors.toList());
		
		meals.forEach(m -> mealRepository.deleteById(m.getId()));
		
		
	}
	

}
