package wedoogift.fr.recruitment.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Gift;
import wedoogift.fr.recruitment.repository.GiftRepository;

@Service
public class GiftService {

	private final GiftRepository giftRepository;
	
	private final TimeManager timeManager;

	@Autowired
	public GiftService(GiftRepository giftRepository, TimeManager timeManager) {
		this.giftRepository = giftRepository;
		this.timeManager = timeManager;
	}

	public List<Gift> getGifts() {
		
		return giftRepository.findAll();
	}

	public Gift getGift(Long id) {
		
		return giftRepository.findById(id);
	}
	
	@Transactional
	public void delete(Long id) {
		giftRepository.deleteById(id);
	}

	public void addGift(String libelle, Integer montant) {
		Gift gift = new Gift();
		gift.setMontant(montant);
		this.giftRepository.save(gift);
	}
	
	@Transactional
	public void updateAllGifts() {
		List<Gift> gifts = giftRepository.findAllActive()
				.stream()
				.filter(g -> g.getEndDate().isBefore(timeManager.getDate()))
				.collect(Collectors.toList());
		
		
	//	gifts.forEach(g -> giftRepository.deleteById(g.getId()));

	gifts.forEach(g -> giftRepository.updateActiveById(false, g.getId()));
		
		
	}
	

}
