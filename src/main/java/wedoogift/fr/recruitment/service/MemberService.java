package wedoogift.fr.recruitment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Gift;
import wedoogift.fr.recruitment.entity.Member;
import wedoogift.fr.recruitment.repository.GiftRepository;
import wedoogift.fr.recruitment.repository.MemberRepository;


@Service
public class MemberService {
	private final MemberRepository memberRepository;
	private final GiftRepository giftRepository;

	@Autowired
	public MemberService(MemberRepository memberRepository, GiftRepository giftRepository) {
		this.memberRepository = memberRepository;
		this.giftRepository = giftRepository;
	}

	public List<Member> getMembers() {
		return memberRepository.findAll();
	}
	
	public Member getMember(Long id) {
		return memberRepository.findById(id);
	}
	
	@Transactional
	public Long addMember(String name) {
		Member m = new Member();
		m.setName(name);
		this.memberRepository.save(m);
		return m.getId();
	}
	
	@Transactional
	public void addGift(Member member, String libelle, Integer montant) {
		Gift gift = new Gift();
		gift.setMontant(montant);
		member.addGift(gift);
		this.giftRepository.save(gift);
	}

}
