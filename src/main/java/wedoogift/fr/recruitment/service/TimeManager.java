package wedoogift.fr.recruitment.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

@Service
public class TimeManager {
	
	private LocalDate date ;
	
	public TimeManager() {
		 date = LocalDate.now();
	}
	
	public void addDays(Long days) {
		date = this.date.plusDays(days);
	}
	
	public String getDateString() {
		return this.date.getMonth()+" "+this.date.getYear();
	}
	
	public LocalDate getDate() {
		return this.date;
	}
	
}
