package wedoogift.fr.recruitment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Meal;

@Transactional
public interface MealRepository extends Repository<Meal, Long> {

	@Modifying
	void deleteById(Long eventId);

	List<Meal> findAll();

	@Modifying
	@Transactional
	void save(Meal gift);

	Meal findById(Long id);
	
	@Modifying
    @Query("update Meal m set m.active = ?1 where m.id = ?2")
    void updateActiveById(boolean active, Long id);
    

    @Query("SELECT m FROM Meal m WHERE m.active = true")
    List<Meal> findAllActive();
    
}
