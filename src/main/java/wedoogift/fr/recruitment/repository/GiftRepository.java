package wedoogift.fr.recruitment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Gift;

@Transactional
public interface GiftRepository extends Repository<Gift, Long> {

	@Modifying
	void deleteById(Long eventId);

	List<Gift> findAll();

	@Modifying
	@Transactional
	void save(Gift gift);

	Gift findById(Long id);
	
	@Modifying
    @Query("update Gift g set g.active = ?1 where g.id = ?2")
    void updateActiveById(boolean active, Long id);
    

    @Query("SELECT g FROM Gift g WHERE g.active = true")
    List<Gift> findAllActive();
    
}
