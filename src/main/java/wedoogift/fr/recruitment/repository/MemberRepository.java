package wedoogift.fr.recruitment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Member;

@Transactional
public interface MemberRepository  extends Repository<Member, Long> {

	@Modifying
	void deleteById(Long eventId);

	List<Member> findAll();
	
	void save(Member member);

	Member findById(Long id);
   

}
