package wedoogift.fr.recruitment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import wedoogift.fr.recruitment.entity.Company;

@Transactional
public interface CompanyRepository  extends Repository<Company, Long> {

	@Modifying
	void deleteById(Long eventId);

	List<Company> findAll();
	
	void save(Company member);

	Company findById(Long id);

   

}
