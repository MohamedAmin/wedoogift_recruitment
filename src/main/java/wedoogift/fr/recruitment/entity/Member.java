package wedoogift.fr.recruitment.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Member {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	@OneToMany(cascade = {CascadeType.ALL})
	private Set<Gift> gifts;
	
	@OneToMany(cascade = {CascadeType.ALL})
	private Set<Meal> meals;

	public Set<Gift> getGifts() {
		return gifts;
	}

	public void setGifts(Set<Gift> gifts) {
		this.gifts = gifts;
	}

	public Set<Meal> getMeals() {
		return meals;
	}

	public void setMeals(Set<Meal> meals) {
		this.meals = meals;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addGift(Gift gift) {
		this.gifts.add(gift);
	}
	
	public void addMeal(Meal meal) {
		this.meals.add(meal);
	}
	
	public Long getId() {
		return id;
	}
	
	/**
	 * Calcule le total des montants des meals
	 * @return le total des montants des meals
	 */
	public Integer getTotalMeal() {
		if(this.meals.isEmpty()) {
			return 0;
		}
		else {
			Integer a = this.meals.stream().mapToInt(o -> o.getMontant()).sum();
			return a;
		}
	}
}
