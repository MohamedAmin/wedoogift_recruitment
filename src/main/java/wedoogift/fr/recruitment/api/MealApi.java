package wedoogift.fr.recruitment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.fr.recruitment.entity.Meal;
import wedoogift.fr.recruitment.service.MealService;

@RestController
@RequestMapping("/api/meal")
public class MealApi{

	private final MealService mealService;

	@Autowired
	public MealApi(MealService mealService) {
		this.mealService = mealService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Meal> findMeals() {
		return mealService.getMeals();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Meal findGiftById(@PathVariable Long id) {
		return mealService.getMeal(id);
	}
	
}
