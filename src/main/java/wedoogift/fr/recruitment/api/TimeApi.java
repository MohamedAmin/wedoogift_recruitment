package wedoogift.fr.recruitment.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.fr.recruitment.service.GiftService;
import wedoogift.fr.recruitment.service.MealService;
import wedoogift.fr.recruitment.service.MemberService;
import wedoogift.fr.recruitment.service.TimeManager;

@RestController
@RequestMapping("/api/time")
public class TimeApi{
	
	private final TimeManager timeManager;
	private final GiftService giftService;
	private final MealService mealService;


	@Autowired
	public TimeApi(GiftService giftService, MemberService membreService, TimeManager timeManager, MealService mealService) {
		this.timeManager = timeManager;
		this.giftService = giftService;
		this.mealService = mealService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String findDate() {
		return timeManager.getDateString();
	}
	
	@RequestMapping(value = "/add/days/{nbDays}", method = RequestMethod.GET)
	public String addDays(@PathVariable Long nbDays) {
		timeManager.addDays(nbDays);
		this.giftService.updateAllGifts();
		this.mealService.updateAllMeal();
		return timeManager.getDateString();
	}
}
