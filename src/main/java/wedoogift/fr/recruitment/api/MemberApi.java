package wedoogift.fr.recruitment.api;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.fr.recruitment.entity.Member;
import wedoogift.fr.recruitment.service.MemberService;

@RestController
@RequestMapping("/api/member")
public class MemberApi{

	private final MemberService membreService;
	
	@Autowired
	public MemberApi(MemberService membreService) {
		this.membreService = membreService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Member> findMember() {
		return membreService.getMembers();
	}
	
	@Transactional
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Long addMember(String name) {
		return membreService.addMember(name);
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Member findMemberById(@PathVariable Long id) {
		return membreService.getMember(id);
	}
	
	@RequestMapping(value = "/{id}/meal/total", method = RequestMethod.GET)
	public Integer findTotalMealMemberById(@PathVariable Long id) {
		return membreService.getMember(id).getTotalMeal();
	}
}
