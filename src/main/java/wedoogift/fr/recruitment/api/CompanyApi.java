package wedoogift.fr.recruitment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.fr.recruitment.entity.Company;
import wedoogift.fr.recruitment.service.CompanyService;

@RestController
@RequestMapping("/api/company")
public class CompanyApi{

	private final CompanyService companyService;
	
	@Autowired
	public CompanyApi(CompanyService companyService) {
		this.companyService = companyService;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Company> findCompanys() {
		return companyService.getCompanys();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Company findCompany(@PathVariable Long id) {
		return companyService.getCompany(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void addCompany(String name) {
		companyService.addCompany(name);
	}
	
	@RequestMapping(value = "/{id}/member/{idMember}/gift", method = RequestMethod.POST)
	public void addGiftToMember(@PathVariable Long id, @PathVariable Long idMember, Integer montant) {
		System.out.println("POST : /"+id+"/member/"+idMember+"/gift, avec comme montant : "+montant);
		companyService.addGiftToMember(id, idMember, montant);
	}
	
	@RequestMapping(value = "/{id}/member/{idMember}/meal", method = RequestMethod.POST)
	public void addMealToMember(@PathVariable Long id, @PathVariable Long idMember, Integer montant) {
		System.out.println("POST : /"+id+"/member/"+idMember+"/meal, avec comme montant : "+montant);
		companyService.addMealToMember(id, idMember, montant);
	}
	
}
