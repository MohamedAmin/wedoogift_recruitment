package wedoogift.fr.recruitment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.fr.recruitment.entity.Gift;
import wedoogift.fr.recruitment.service.GiftService;

@RestController
@RequestMapping("/api/gift")
public class GiftApi{

	private final GiftService giftService;

	@Autowired
	public GiftApi(GiftService giftService) {
		this.giftService = giftService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Gift> findGifts() {
		return giftService.getGifts();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Gift findGiftById(@PathVariable Long id) {
		return giftService.getGift(id);
	}
	
	
}
