package wedoogift.fr.recruitment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class WeDooGiftRecruitmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeDooGiftRecruitmentApplication.class, args);
	}
}
