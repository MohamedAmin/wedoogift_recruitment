Feature: Member API
  Member API

 Scenario: GET MEMBERS
 	When nous fesons un appel a /api/member
 	Then le code retour est 200 
 	And nous recuperons 4 member

 Scenario: GET MEMBER_1
 	When nous fesons un appel a /api/member/1001
 	Then le code retour pour un member est 200 
 	And nous recuperons le membre dont le nom est Mohamed
 	
 Scenario: GET MEMBER_2
 	When nous fesons un appel a /api/member/1002
 	Then le code retour pour un member est 200 
 	And nous recuperons le membre dont le nom est Richard
 	
	Scenario: GET MEMBER_3
 	When nous fesons un appel a /api/member/1003
 	Then le code retour pour un member est 200 
 	And nous recuperons le membre dont le nom est Valentine