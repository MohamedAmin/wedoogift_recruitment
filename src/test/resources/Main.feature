Feature: API

 Scenario: Rajout Gift
 	Given the member John with id 1 and company Tesla with id 1 exist for this context
 	And company with id 1 send a gift to member with id 1 from a value of 70€
 	When the member with id 1 check this gift
 	Then this gift have a value is 70€ and he is active
 
 Scenario: Verification du gift apres quelques jours
  Given 3 days have passed
	When the member with id 1 check this gift
 	Then this gift have a value is 70€ and he is active
 	
 Scenario: Verification du gift apres 365 jours
  Given 365 days have passed
	When the member with id 1 check this gift
 	Then this gift have a value is 70€ and he is not active
 	
 Scenario: Rajout d'un Meal
 	Given the member John with id 1 and company Tesla with id 1 exist
 	And total values of this meals is 0€
 	And company with id 1 send a meal to member with id 1 from a value of 70€
 	When the member with id 1 check this meal
 	Then total values of this meals is 70€
 
 Scenario: Verification du meal apres 365 jours
  Given 300 days have passed
	When the member with id 1 check this meal
 	Then total values of this meals is 0€