package wedoogift.fr.recruitment.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import wedoogift.fr.recruitment.entity.Member;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MemberStep {
	
	private static String BASE_URL = "http://localhost:";
	private static String PATH_URL = "/api/";
	
	@LocalServerPort
	private int port;
		
    private WireMockServer wireMockServer = new WireMockServer(8080);
        
    private ResponseEntity<Member[]> members;
    
    private ResponseEntity<Member> member;

	
	@Before
	private void initMockServcer() {
		wireMockServer.start();
	}

	@When("nous fesons un appel a \\/api\\/member")
	public void testJddOkMembers() {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
	
		members = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/", Member[].class);

	}

	@When("nous fesons un appel a \\/api\\/member\\/{int}")
	public void testJddOkMember(Integer id) {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
	
		member = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/"+id, Member.class);

	}
	
	@Then("le code retour pour un member est 200")
	public void checkStatutCodeOk() {
		assertEquals(member.getStatusCodeValue(), HttpStatus.OK_200);
	}
	
	@Then("le code retour est 200")
	public void checkStatutCodeOk2() {
		assertEquals(members.getStatusCodeValue(), HttpStatus.OK_200);
	}
	
	@And("nous recuperons {int} member")
	public void checkNbOfMember(Integer nbMember){
		assertEquals(members.getBody().length, nbMember);
	}
	
	@And("nous recuperons le membre dont le nom est {word}")
	public void checkNameOfMember(String name){
		assertEquals(member.getBody().getName(), name);
	}
	

}
