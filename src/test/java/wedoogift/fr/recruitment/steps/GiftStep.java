package wedoogift.fr.recruitment.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import wedoogift.fr.recruitment.entity.Company;
import wedoogift.fr.recruitment.entity.Gift;
import wedoogift.fr.recruitment.entity.Member;

@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GiftStep {
	
	private static String BASE_URL = "http://localhost:";
	private static String PATH_URL = "/api/";
	
	@LocalServerPort
	private int port;
		
    private WireMockServer wireMockServer = new WireMockServer(8080);
    
    private Set<Gift> gifts;
	
	@Before
	private void initMockServcer() {
		wireMockServer.start();
	}

	@Given("the member {word} with id {int} and company {word} with id {int} exist for this context")
	public void testJddOk(String mName, Integer mId, String cName, Integer cId) {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
	
		Member john = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/"+mId, Member.class).getBody();
		Company tesla = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/company/"+cId, Company.class).getBody();

		assertEquals(john.getName(), mName);
		assertEquals(tesla.getName(), cName);	
	}
	
	
	@Given("{int} days have passed")
	public void avancerLeJour(Integer nbJour) {
		TestRestTemplate testRestTemplate = new TestRestTemplate();

		ResponseEntity<String> response = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/time/add/days/"+nbJour, null);
		assertEquals(response.getStatusCodeValue(), HttpStatus.OK_200);
	}
	
	
	@And("company with id {int} send a gift to member with id {int} from a value of {int}€")
	public void testAddGiftToMember(Integer companyId, Integer memberId, Integer valeur) {
		
		TestRestTemplate testRestTemplate = new TestRestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("montant", String.valueOf(valeur));

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		
		ResponseEntity<String> response = testRestTemplate.postForEntity( BASE_URL+port+PATH_URL+"/company/"+companyId+"/member/"+memberId+"/gift", request,String.class);
		
		assertEquals(response.getStatusCodeValue(), HttpStatus.OK_200);
	}
	
	@When("the member with id {int} check this gift")
	public void testGetGifts(Integer mId){
		TestRestTemplate testRestTemplate = new TestRestTemplate();

		gifts = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/"+mId, Member.class).getBody().getGifts();
	}
	
	@Then("this gift have a value is {int}€ and he is active")
	public void checkGiftMontantActive(Integer montant){
		Gift g = gifts.stream().findFirst().get();
		assertEquals(g.getMontant(), montant);
		assertEquals(g.isActive(), true);
	}
	
	
	@Then("this gift have a value is {int}€ and he is not active")
	public void checkGiftMontantPasActive(Integer montant){
		Gift g = gifts.stream().findFirst().get();
		assertEquals(g.getMontant(), montant);
		assertEquals(g.isActive(), false);
	}
	
	
}
