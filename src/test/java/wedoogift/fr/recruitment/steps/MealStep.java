package wedoogift.fr.recruitment.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import wedoogift.fr.recruitment.entity.Company;
import wedoogift.fr.recruitment.entity.Member;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MealStep {
	
	private static String BASE_URL = "http://localhost:";
	private static String PATH_URL = "/api/";
	
	@LocalServerPort
	private int port;
		
    private WireMockServer wireMockServer = new WireMockServer(8080);
    
    private Integer totalMeal;
    
	@Before
	private void initMockServcer() {
		wireMockServer.start();
	}
	
	@Given("the member {word} with id {int} and company {word} with id {int} exist")
	public void testJddOk(String mName, Integer mId, String cName, Integer cId) {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
	
		Member john = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/"+mId, Member.class).getBody();
		Company tesla = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/company/"+cId, Company.class).getBody();
		
		assertEquals(john.getName(), mName);
		assertEquals(tesla.getName(), cName);
		this.totalMeal = john.getTotalMeal();

	}
	
	@And("company with id {int} send a meal to member with id {int} from a value of {int}€")
	public void testAddMealToMember(Integer companyId, Integer memberId, Integer valeur) {
		
		TestRestTemplate testRestTemplate = new TestRestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("montant", String.valueOf(valeur));

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		
		ResponseEntity<String> response = testRestTemplate.postForEntity( BASE_URL+port+PATH_URL+"company/"+companyId+"/member/"+memberId+"/meal", request,String.class);
		
		assertEquals(response.getStatusCodeValue(), HttpStatus.OK_200);
	}
	
	@When("the member with id {int} check this meal")
	public void testGetMealTotal(Integer mId){
		TestRestTemplate testRestTemplate = new TestRestTemplate();
		totalMeal = testRestTemplate.getForEntity(BASE_URL+port+PATH_URL+"/member/"+mId+"/meal/total", Integer.class).getBody();
	}
	
	
	
	@Then("total values of this meals is {int}€")
	public void checkMealMontant(Integer montant){
		assertEquals(totalMeal, montant);
	}
	
	
	
}
